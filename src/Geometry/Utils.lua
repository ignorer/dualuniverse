-- TODO: vector/line/plane rotation
-- TODO: projection of line on plane
-- TODO: vector/point/line/plane reflection
-- TODO: line/plane intersection
-- TODO: calculation of angle between: 1) two vectors/lines/planes, 2) line and plane

M = {
	PI = 3.1415926535
}

--========================ANGLE========================--
M.Angle = {}

function M.Angle:new(radians)
	if type(radians) ~= "number" then
		error "Angle: cannot create instance (constructor arguments must be a number"
	end
	local newObj = {value = (radians == nil and 0 or radians)}
	self.__index = self
	return setmetatable(newObj, self)
end

function M.Angle:__add(another)
	if another.__index ~= M.Angle then
		error "Angle: cannot add (invalid argument type)"
	end
	return M.Angle:new(self.value + another.value)
end

function M.Angle:__sub(another)
	if another.__index ~= M.Angle then
		error "Angle: cannot subtract (invalid argument type)"
	end
	return M.Angle:new(self.value - another.value)
end

function M.Angle:__unm()
	return M.Angle:new(-self.value)
end

function M.Angle:toDegrees()
	return self.value * 180 / M.PI
end

function M.Angle:__tostring()
	return self.value
end

--========================VECTOR========================--
M.Vector = {}

function M.Vector:new(x, y, z)
	if type(x) ~= "number" or type(y) ~= "number" or type(z) ~= "number" then
		error "Vector: cannot create instance (constructor arguments must be numbers)"
	end
	local newObj = {x = x, y = y, z = z}
	self.__index = self
	return setmetatable(newObj, self)
end

function M.Vector:__add(another)
	if another.__index ~= M.Vector then
		error "Vector: cannot add (invalid argument type"
	end
	return M.Vector:new(another.x + self.x, another.y + self.y, another.z + self.z)
end

function M.Vector:__sub(another)
	if another.__index ~= M.Vector then
		error "Vector: cannot subtract (invalid argument type"
	end
	return M.Vector:new(another.x - self.x, another.y - self.y, another.z - self.z)
end

function M.Vector:__unm()
	return M.Vector:new(-self.x, -self.y, -self.z)
end

function M.Vector:__mul(k)
	if type(k) ~= "number" then
		error "Vector: cannot multiply (multiplier must be a number)"
	end
	return M.Vector:new(self.x * k, self.y * k, self.z * k)
end

function M.Vector:__div(k)
	if type(k) ~= "number" then
		error "Vector: cannot multiply (divider must be a number)"
	elseif k == 0 then
		error "Vector: cannot divide (division by 0 is not allowed)"
	end
	return self * (1 / k)
end

function M.Vector:scalarProduct(another)
	if another.__index ~= M.Vector then
		error "Vector: cannot calculate scalar product (invalid argument type)"
	end
	return another.x * self.x + another.y * self.y + another.z * self.z
end

function M.Vector:vectorProduct(another)
	if another.__index ~= M.Vector then
		error "Vector: cannot calculate vector product (invalid argument type)"
	end
	return M.Vector:new(self.y * another.z - self.z * another.y,
		self.z * another.x - self.x * another.z,
		self.x * another.y - self.y * another.x	)
end

function M.Vector:length()
	return (self.x ^ 2 + self.y ^ 2 + self.z ^ 2) ^ 0.5
end

function M.Vector:__tostring()
	return "[" .. self.x .. ", " .. self.y .. ", " .. self.z .. "]"
end

function M.Vector:normalize()
	local k = self:length()
	if k == 0 then
		error "Vector: cannot normalize (vector length is 0)"
	end
	return self / k
end

function M.Vector:isCollinear(vector)
	if vector.__index ~= M.Vector then
		error "Vector: cannot check for collinearity (invalid argument type)"
	else
		return self:vectorProduct(vector):length() == 0
	end
end

function M.Vector:isComplanar(u, v)
	if u.__index ~= M.Vector or v.__index ~= M.Vector then
		error "Vector: cannot check for complanarity (invalid argument types)"
	else
		return self:scalarProduct(u:vectorProduct(v)) == 0
	end
end

--========================POINT========================--
M.Point = {}

function M.Point:new(x, y, z)
	if type(x) ~= "number" or type(y) ~= "number" or type(z) ~= "number" then
		error "Point: cannot create instance (constructor arguments must be numbers)"
	end
	local newObj = {x = x, y = y, z = z}
	self.__index = self
	return setmetatable(newObj, self)
end

function M.Point:__add(vec)
	if vec.__index ~= M.Vector then
		error "Point: cannot add (invalid argument type)"
	end
	return M.Point:new(self.x + vec.x, self.y + vec.y, self.z + vec.z)
end

function M.Point:__sub(vecOrPoint)
	if vecOrPoint.__index == M.Vector then
		return self + (-vecOrPoint)
	elseif vecOrPoint.__index == M.Point then
		return M.Vector:new(self.x - vecOrPoint.x, self.y - vecOrPoint.y, self.z - vecOrPoint.z)
	else
		error "Point: cannot subtract (invalid argument type)"
	end
end

function M.Point:__tostring()
	return "(" .. self.x .. ", " .. self.y .. ", " .. self.z .. ")"
end

-- obj must be a plane or a line
function M.Point:project(obj)
	if obj.__index == M.Line then
		local u = obj:getDirectionalVector()
		local v = self - obj:getPoint()
		local h = u:vectorProduct(v):vectorProduct(u)
		if h:length() == 0 then
			return self
		end
		h = h:normalize()
		return self - h * h:scalarProduct(v)
	elseif obj.__index == M.Plane then
		local normal = obj:getNormal()
		local v = self - obj:getPoint()
		return self - normal * v:scalarProduct(normal)
	else
		error "Point: cannot project (invalid argument type)"
	end
end

-- obj must be a dot, line or a plane
function M.Point:distanceTo(obj)
	if (obj.__index == M.Point) then
		return ((obj.x - self.x) ^ 2 + (obj.y - self.y) ^ 2 + (obj.z - self.z) ^ 2) ^ 0.5
	elseif (obj.__index == M.Line) then
		return (self - self:project(obj)):length()
	elseif (obj.__index == M.Plane) then
		return (self - self:project(obj)):length()
	else
		error "Point: cannot calculate distance to object (invalid argument type)"
	end
end

--========================LINE========================--
-- line defines with point and directional vector
M.Line = {}

function M.Line:new(point, directionalVector)
	if point.__index ~= M.Point or directionalVector.__index ~= M.Vector then
		error "Line: cannot create instance (invalid argument type)"
	elseif directionalVector:length() == 0 then
		error "Line: cannot create instance (direction vector with zero length is not allowed)"
	end
	local n = directionalVector:normalize()
	local newObj = {x0 = point.x, y0 = point.y, z0 = point.z, dx = n.x, dy = n.y, dz = n.z}
	self.__index = self
	return setmetatable(newObj, self)
end

function M.Line:getPoint()
	return M.Point:new(self.x0, self.y0, self.z0)
end

function M.Line:setPoint(point)
	if point.__index ~= M.Point then
		error "Line: cannot set point (invalid argument type)"
	end
	self.x0, self.y0, self.z0 = point.x, point.y, point.z
end

function M.Line:getDirectionalVector()
	return M.Vector:new(self.dx, self.dy, self.dz)
end

function M.Line:setDirectionalVector(vector)
	if vector.__index ~= M.Vector then
		error "Line: cannot set directional vector (invalid argument type)"
	elseif vector:length() == 0 then
		error "Line: cannot set directional vector (direction vector with zero length is not allowed)"
	end
	self.dx, self.dy, self.dz = vector.x, vector.y, vector.z
end

function M.Line:distanceTo(obj)
	if obj.__index == M.Point then
		return obj:distanceTo(self)
	elseif obj.__index == M.Line then
		local u = self:getDirectionalVector()
		local v = self:getDirectionalVector()
		local point = self:getPoint()
		if u:isCollinear(v) then
			return point:distanceTo(obj)
		else
			return M.Point:distanceTo(M.Plane:new(obj:getPoint(), v:vectorProduct(u)))
		end
	elseif obj.__index == M.Plane then
		if self:getDirectionalVector():scalarProduct(obj:getNormal()) == 0 then
			return (self:getPoint() - obj:getPoint()):scalarProduct(obj:getNormal())
		else
			return 0
		end
	else
		error "Line: cannot calculate distance to object (invalid argument type)"
	end
end

--========================PLANE========================--
-- plane defines with point and normal vector
M.Plane = {}

function M.Plane:new(point, vector)
	if point.__index ~= M.Point or vector.__index ~= M.Vector then
		error "Plane: cannot create instance (invalid argument types)"
	elseif vector:length() == 0 then
		error "Plane: cannot create instance (normal vector with zero length is not allowed)"
	end
	local normal = vector:normalize()
	local newObj = {x0 = point.x, y0 = point.y, z0 = point.y, nx = normal.x, ny = normal.y, nz = normal.z}
	self.__index = self
	return setmetatable(newObj, self)
end

function M.Plane:getPoint()
	return M.Point:new(self.x0, self.y0, self.z0)
end

function M.Plane:setPoint(point)
	if point.__index ~= M.Point then
		error "Plane: cannot set point (invalid argument type)"
	end
	self.x0, self.y0, self.z0 = point.x, point.y, point.z
end

function M.Plane:getNormal()
	return M.Vector:new(self.nx, self.ny, self.nz)
end

function M.Plane:setNormal(normal)
	if normal.__index ~= M.Vector then
		error "Plane: cannot set normal vector (invalid argument type)"
	end
	self.nx, self.ny, self.nz = normal.x, normal.y, normal.z
end

function M.Plane:distanceTo(obj)
	if obj.__index == M.Point then
		return obj:distanceTo(self)
	elseif obj.__index == M.Line then
		return obj:distanceTo(self)
	elseif obj.__index == M.Plane then
		if self:getNormal():isCollinear(obj:getNormal()) then
			return self:getPoint():distanceTo(obj)
		else
			return 0
		end
	else
		error "Plane: cannot calculate distance to object (invalid argument type)"
	end
end

--=====================================================--
return M